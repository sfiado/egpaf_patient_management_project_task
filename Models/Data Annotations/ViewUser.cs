﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Patient_Management_Project.Models
{
    public class ViewUser
    {
        public long UserID { get; set; }
        public string Username { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(60, MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(60, MinimumLength = 3)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "That seems like an invalid email address")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Initial Password")]
        public string InitialPassword { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [Compare("InitialPassword")]
        public string ConfirmNewPassword { get; set; }
        public bool Active { get; set; }

        [Required]
        [Display(Name = "User Role")]
        public string UserRole { get; set; }
        public string DateCreated { get; set; }
        public string LastLogin { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class ResetPassword
    {
        public int UserID { get; set; }
        public string Username { get; set; }

        [Required]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "Confirm New Password")]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }
    }

    public interface ILogin
    {
        string Password { get; set; }
        string Username { get; set; }

    }
    public class Login : ILogin
    {
        [Required]
        [Display(Name = "Username")]
        [StringLength(50, MinimumLength = 4)]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
  
    public class ViewChangePassword
    {
        [Required]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword")]
        [Display(Name = "Confirm New Password")]
        public string ConfirmNewPassword { get; set; }
    }

}