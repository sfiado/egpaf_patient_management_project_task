﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Patient_Management_Project.Models
{
    public class ViewPatient
    {
        public int PatientAutoID { get; set; }

        [Required]
        [Display(Name = "Identification Number")]
        public string PatientIDNumber { get; set; }

        [Required]
        [Display(Name = "Identification Type")]
        public string PatientIDType { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "Birth Date")]
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "Current Address")]
        public string CurrentAddress { get; set; }

        [Required]
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        
    }
    public class ViewMedicalVisit
    {
        public int VisitID { get; set; }
        public int PatientAutoID { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "Visit Date")]
        public DateTime VisitDate { get; set; }

        [Required]
        [Display(Name = "Weight (kg)")]
        public decimal Weight { get; set; }

        [Required]
        [Display(Name = "Height (cm)")]
        public decimal Height { get; set; }

        [Required]
        [Display(Name = "Temperature")]
        public decimal Temperature { get; set; }

        [Required]
        [Display(Name = "Diagnosis")]
        public int DiagnosisCodeID { get; set; }
        public string DiagnosisName { get; set; }
        public string PatientName { get; set; }
        public string PatientIDNumber { get; set; }
        public string CreatedBy { get; set; }
    }
    public class ViewDiagnosisCode
    {
        public int DiagnosisCodeID { get; set; }
        public string DiagnosisCodeDescription { get; set; }
    }
}