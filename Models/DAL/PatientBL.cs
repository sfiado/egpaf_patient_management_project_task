﻿using Patient_Management_Project.EF;
using Patient_Management_Project.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Patient_Management_Project.Helpers.SessionManager;


namespace Patient_Management_Project.Models.DAL
{
    public class PatientBL
    {
        private string loggedInUser = LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username;

        public struct ActionResult
        {
            public bool IsSuccess;
            public string SuccessMessage;
            public string PossibleErrorMessage;
        }
        public struct PatientMetaData
        {
            public int PatientAutoID;
            public string PatientIDNumber;
            public string PatientIDType;
            public string FirstName;
            public string LastName;
            public string Gender;
            public DateTime BirthDate;
            public string CurrentAddress;
            public string Occupation;
        }
        public struct MedicalVisitMetaData
        {
            public int PatientAutoID;
            public DateTime VisitDate;
            public decimal Weight;
            public decimal Height;
            public decimal Temperature;
            public int DiagnosisCodeID;
        }
        

        /// <summary>
        /// Gets registered patients
        /// </summary>
        /// <returns></returns>

        public IEnumerable<ViewPatient> GetRegisteredPatient()
        {

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                var registeredPatients = new List<ViewPatient>();
                try
                {
                    var patients = context.Patients.Select(x => new
                    {
                        x.PatientAutoID,
                        x.PatientIDNumber,
                        x.PatientIDType,
                        x.FirstName,
                        x.LastName,
                        x.Gender,
                        x.Birthdate,
                        x.CurrentAddress,
                        x.Occupation,
                        x.CreatedOn,
                        x.Active
                    })

                    .OrderByDescending(x => x.CreatedOn);

                    if (patients.Count() > 0)
                    {
                        foreach (var patient in patients)
                        {
                            var thisPatient = new ViewPatient
                            {
                                PatientAutoID = patient.PatientAutoID,
                                PatientIDType= patient.PatientIDType,
                                PatientIDNumber = patient.PatientIDNumber,
                                FirstName = patient.FirstName,
                                LastName = patient.LastName,
                                Gender = patient.Gender,
                                BirthDate = patient.Birthdate,
                                Active=patient.Active,
                                CurrentAddress=patient.CurrentAddress,
                                Occupation=patient.Occupation,
                            };
                            registeredPatients.Add(thisPatient);
                        }
                    }
                    return registeredPatients;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }


        /// <summary>
        /// Method that registers a new patient
        /// </summary>
        /// <param name="patientInfo"></param>
        /// <returns></returns>
        public ActionResult RegisterPatient(PatientMetaData patientInfo)
        {
            var response = new ActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    
                        var thisPatient = new Patient()
                        {
                            PatientAutoID = context.Patients.Any() ? context.Patients.Max(x => x.PatientAutoID) + 1 : 1,
                            FirstName = patientInfo.FirstName,
                            LastName = patientInfo.LastName,
                            PatientIDNumber=patientInfo.PatientIDNumber,
                            PatientIDType=patientInfo.PatientIDType,
                            Gender=patientInfo.Gender,
                            CurrentAddress=patientInfo.CurrentAddress,
                            Birthdate=patientInfo.BirthDate,
                            Occupation=patientInfo.Occupation,
                            Active=true,
                            CreatedOn = DateTime.Now,
                            CreatedBy = loggedInUser,
                            
                        };

                        context.Patients.Add(thisPatient);
                        context.SaveChanges();

                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Register patient-" + patientInfo.PatientIDNumber + "|" + DateTime.Now + "|" + loggedInUser);

                    

                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method that modifies a patient
        /// </summary>
        /// <param name="patientInfo"></param>
        /// <returns></returns>
        public ActionResult ModifyPatient(PatientMetaData patientInfo)
        {
            var response = new ActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {

                    var thisPatient = context.Patients.SingleOrDefault(x => x.PatientAutoID == patientInfo.PatientAutoID);
                    if (thisPatient != null)
                    {

                        thisPatient.FirstName = patientInfo.FirstName;
                        thisPatient.LastName = patientInfo.LastName;
                        thisPatient.PatientIDNumber = patientInfo.PatientIDNumber;
                        thisPatient.PatientIDType = patientInfo.PatientIDType;
                        thisPatient.Gender = patientInfo.Gender;
                        thisPatient.CurrentAddress = patientInfo.CurrentAddress;
                        thisPatient.Birthdate = patientInfo.BirthDate;
                        thisPatient.Occupation = patientInfo.Occupation;
                        thisPatient.ModifiedBy = loggedInUser;
                        thisPatient.ModifiedOn = DateTime.Now;

                        context.SaveChanges();


                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Modify Patient-" + thisPatient.PatientIDNumber + "|" + DateTime.Now + "|" + loggedInUser);

                    }

                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method that deactivates (set Active=false)  a patient
        /// </summary>
        /// <param name="patientAutoID"></param>
        /// <returns></returns>
        public ActionResult DeactivatePatient(int patientAutoID)
        {
            var response = new ActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {


                    var thispatient = context.Patients.SingleOrDefault(x => x.PatientAutoID == patientAutoID);
                    if (thispatient != null)
                    {
                        thispatient.Active = false;
                        thispatient.ModifiedOn = DateTime.Now;
                        thispatient.ModifiedBy = loggedInUser;

                        context.SaveChanges();


                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Deactivate Patient-" + thispatient.PatientIDNumber + "|" + DateTime.Now + "|" + loggedInUser);

                    }


                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }


        /// <summary>
        /// Method that activates (set Active=true)  a patient
        /// </summary>
        /// <param name="patientAutoID"></param>
        /// <returns></returns>
        public ActionResult ActivatePatient(int patientAutoID)
        {
            var response = new ActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {


                    var thispatient = context.Patients.SingleOrDefault(x => x.PatientAutoID == patientAutoID);
                    if (thispatient != null)
                    {
                        thispatient.Active = true;
                        thispatient.ModifiedOn = DateTime.Now;
                        thispatient.ModifiedBy = loggedInUser;

                        context.SaveChanges();


                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Activate Patient-" + thispatient.PatientIDNumber + "|" + DateTime.Now + "|" + loggedInUser);

                    }


                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method that deletes a patient
        /// </summary>
        /// <param name="patientAutoID"></param>
        /// <returns></returns>
        public ActionResult DeletePatient(int patientAutoID)
        {
            var response = new ActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {


                    var thisPatient = context.Patients.SingleOrDefault(x => x.PatientAutoID == patientAutoID);
                    if (thisPatient != null)
                    {
                        context.Patients.Remove(thisPatient);

                        context.SaveChanges();

                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Delete Patient-" + thisPatient.PatientIDNumber + "|" + DateTime.Now + "|" + loggedInUser);
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }



        /// <summary>
        /// Gets registered patients
        /// </summary>
        /// <returns></returns>

        public IEnumerable<ViewMedicalVisit> GetPatientRecord()
        {

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                var patientsRecords = new List<ViewMedicalVisit>();
                try
                {
                    var patientVisits = context.vw_PatientHealthRecord.Select(x => new
                    {
                        x.PatientAutoID,
                        x.PatientIDNumber,
                        x.PatientIDType,
                        x.FirstName,
                        x.LastName,
                        x.Gender,
                        x.Birthdate,
                        x.CurrentAddress,
                        x.Occupation,
                        x.VisitDate,
                        x.Weight,
                        x.Height,
                        x.Temperature,
                        x.DiagnosisCodeDescription,
                        x.CreatedBy
                    })

                    .OrderByDescending(x => x.VisitDate);

                    if (patientVisits.Count() > 0)
                    {
                        foreach (var visit in patientVisits)
                        {
                            var thisVisit = new ViewMedicalVisit
                            {
                                PatientAutoID=visit.PatientAutoID,
                                VisitDate=visit.VisitDate,
                                PatientIDNumber = visit.PatientIDNumber,
                                PatientName = visit.FirstName + " " + visit.LastName,
                                Weight = visit.Weight,
                                Height = visit.Height,
                                Temperature = visit.Temperature,
                                DiagnosisName = visit.DiagnosisCodeDescription,
                                CreatedBy=visit.CreatedBy
                                
                            };
                            patientsRecords.Add(thisVisit);
                        }
                    }
                    return patientsRecords;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }


        /// <summary>
        /// Method that registers a new patient
        /// </summary>
        /// <param name="medicalRecordInfo"></param>
        /// <returns></returns>
        public ActionResult NewMedicalRecord(MedicalVisitMetaData medicalRecordInfo)
        {
            var response = new ActionResult();
            try
            {

                using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
                {

                    using (var dbContextTxn = context.Database.BeginTransaction())
                    {
                        if (medicalRecordInfo.VisitDate > DateTime.Now)
                        {
                            response.IsSuccess = false;
                            response.PossibleErrorMessage = Messages.INVALID_DATE;
                            Common.LogEvent(response.PossibleErrorMessage + "|Create Medical Visit Record-" + DateTime.Now + "|" + loggedInUser);
                        }
                        else
                        {
                            var thisPatientVisit = new PatientVisit()
                            {
                                VisitID = context.PatientVisits.Any() ? context.PatientVisits.Max(x => x.VisitID) + 1 : 1,
                                PatientAutoID = medicalRecordInfo.PatientAutoID,
                                VisitDate = medicalRecordInfo.VisitDate,
                                CreatedOn = DateTime.Now,
                                CreatedBy = loggedInUser,

                            };

                            var thisPatientVital = new PatientVital()
                            {
                                VitalsID = context.PatientVitals.Any() ? context.PatientVitals.Max(x => x.VitalsID) + 1 : 1,
                                VisitID = context.PatientVisits.Any() ? context.PatientVisits.Max(x => x.VisitID) + 1 : 1,
                                Weight = medicalRecordInfo.Weight,
                                Height = medicalRecordInfo.Height,
                                Temperature = medicalRecordInfo.Temperature,
                                CreatedOn = DateTime.Now,
                                CreatedBy = loggedInUser,
                            };

                            var thisPatientDiagnosis = new PatientDiagnosi()
                            {
                                DiagnosisID = context.PatientDiagnosis.Any() ? context.PatientDiagnosis.Max(x => x.DiagnosisID) + 1 : 1,
                                VisitID = context.PatientVisits.Any() ? context.PatientVisits.Max(x => x.VisitID) + 1 : 1,
                                DiagnosisCodeID = medicalRecordInfo.DiagnosisCodeID,
                                CreatedOn = DateTime.Now,
                                CreatedBy = loggedInUser,
                            };

                            context.PatientVisits.Add(thisPatientVisit);
                            context.PatientVitals.Add(thisPatientVital);
                            context.PatientDiagnosis.Add(thisPatientDiagnosis);
                            context.SaveChanges();
                            dbContextTxn.Commit();

                        }
                        
                    }


                }

            }
            catch (Exception ex)
            {
                Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                throw;
            }

            return response;
        }

        /// <summary>
        /// Gets registered patients
        /// </summary>
        /// <returns></returns>

        public IEnumerable<ViewDiagnosisCode> GetDiagnosisCode()
        {

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                var diagnosisCodes = new List<ViewDiagnosisCode>();
                try
                {
                    var codes = context.DiagnosisCodes.Select(x => new
                    {
                        x.DiagnosisCodeID,
                        x.DiagnosisCodeDescription,
                        
                    });

                    if (codes.Count() > 0)
                    {
                        foreach (var code in codes)
                        {
                            var thisCode = new ViewDiagnosisCode
                            {
                                DiagnosisCodeID = code.DiagnosisCodeID,
                                DiagnosisCodeDescription = code.DiagnosisCodeDescription,
                            };
                            diagnosisCodes.Add(thisCode);
                        }
                    }
                    return diagnosisCodes;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + loggedInUser);
                    throw;
                }
            }
        }



    }
}