﻿using Patient_Management_Project.EF;
using Patient_Management_Project.Functions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static Patient_Management_Project.Helpers.SessionManager;

namespace Patient_Management_Project.Models.DAL
{
    public class Authentication
    {
        
        public struct AuthenticationMetaData
        {
            public string Username;
            public string Password;
            public string SelectedDatabase;
        }

        public struct CreateUserResponseMetaData
        {
            public string Username;
            public string PossibleErrorMessage;
        }

        public struct ResetUserPasswordMetaData
        {
            public int UserID;
            public string NewPassword;
            public string ConfirmNewPassword;
            public string Username;
        }
        public struct ModifyUserMetaData
        {
            public long UserID;
            public string Username;
            public string FirstName;
            public string LastName;
            public string EmailAddress;
            public string UserRole;
            
        }

        public struct UserMetaData
        {
            public string Username;
            public string FirstName;
            public string LastName;
            public string EmailAddress;
            public string UserPassword;
            public string UserRole;
            public DateTime CreatedOn;
            public string CreatedBy;
            public bool Active;
        }


        public struct AuthenticationResponse
        {
            public bool IsValidLogin;
            public string PossibleErrorMessage;
            public string Username;
            public string UserRole;
            public int? UserID;
            public string FirstName;
            public string LastName;
            public int? FirstLogin;
            public int? Active;
            public DateTime? PasswordExpiryDate;
            public string EmailAddress;
            public bool HasPasswordExpired;
            public DateTime LastLogin;
            public bool IsLocked;
            public DateTime UnlockTime;
        }

        public struct LockResponse
        {
            public bool IsLocked;
            public DateTime? UnlockTime;
        }

        public struct DeleteUserMetaData
        {
            public string DeletedBy;
            public string DeletedUser;
            public DateTime DeleteTime;
        }

        public struct ModifyActionResult
        {
            public bool IsSuccess;
            public string SuccessMessage;
            public string PossibleErrorMessage;
        }

        /// <summary>
        /// Method gets all users on the system except the logged in user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IEnumerable<ViewUser> GetUsers(int userID)
        {
            var users = new List<ViewUser>();
            try
            {
                using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
                {
                    var dbResponse = context.uspGetUsers(userID).ToList();
                    if (dbResponse.Count > 0)
                    {
                        foreach (var response in dbResponse)
                        {
                            var thisUser = new ViewUser
                            {
                                DateCreated = Helpers.IsNull(Helpers.ConvertToFriendlyDate(response.DateCreated), string.Empty),
                                EmailAddress = Helpers.IsNull(response.EmailAddress, string.Empty),
                                FirstName = Helpers.IsNull(response.FirstName, string.Empty),
                                LastName = Helpers.IsNull(response.LastName, string.Empty),
                                LastLogin = Helpers.IsNull(response.LastLogin, string.Empty),
                                Active = response.Active,
                                UserID = Helpers.IsNull(response.UserID, 0),
                                Username = Helpers.IsNull(response.Username, string.Empty),
                                UserRole =Helpers.Decrypt(Helpers.IsNull(response.UserRole, string.Empty)),
                            };
                            users.Add(thisUser);
                        }
                        
                    }
                    return users.OrderByDescending(x => x.UserID);
                }
            }
            catch (Exception ex)
            {
                Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                throw;
            }
        }

        /// <summary>
        /// Method logs user login activity in the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public void LogActivity(string username, string IPAddress, bool isSuccess)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspLogLoginActivity(context.LoginActivities.Any() ? context.LoginActivities.Max(x => x.AutoID) + 1 : 1,username, IPAddress, isSuccess, DateTime.Now);
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    
                }
            }
        }

        /// <summary>
        /// Method unlocks user profile if locked out
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public void UnlockProfile(string username)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspUnlockProfile(username);
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                }
            }
        }

        /// <summary>
        /// Method locks user profile when they exceed login attempts
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public void LockProfile(string username, DateTime unlockTime)
        {
            
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspLockProfile(username, unlockTime);
                }
                   
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                }
            }
        }

        /// <summary>
        /// Method deletes a user profile in the system
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public bool DeleteUser(DeleteUserMetaData userInfo)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspDeleteUser(userInfo.DeletedBy, userInfo.DeletedUser, userInfo.DeleteTime);
                    return true;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                    throw;
                }
            }
        }


        /// <summary>
        /// Method that resets a user password
        /// </summary>

        /// <param name="passwordInfo"></param>

        /// <returns></returns>

        public ModifyActionResult ResetPassword(ResetUserPasswordMetaData passwordInfo)
        {
            var response = new ModifyActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    int attempts = 0;

                    if (passwordInfo.ConfirmNewPassword != passwordInfo.NewPassword)
                    {
                        response.IsSuccess = false;
                    }
                    else
                    {
                        var thisUser = context.Users.SingleOrDefault(x => x.UserID == passwordInfo.UserID);
                        if (thisUser != null)
                        {
                            thisUser.Password = Helpers.HashPassword(passwordInfo.NewPassword);
                            thisUser.FirstLogin = true;
                            thisUser.PasswordExpiryDate = null;
                            thisUser.Locked = false;
                            thisUser.UnlockTime = null;
                            thisUser.ModifiedOn = DateTime.Now;
                            thisUser.ModifiedBy = LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username;
                        }

                        context.SaveChanges();

                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Reset Password-" + passwordInfo.Username + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                        LoginAttemptsCache.TryRemove(passwordInfo.Username, out attempts);
                    }



                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                    throw;
                }
            }
        }

        /// <summary>
        /// Method that modifies a user profile
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>

        public ModifyActionResult ModifyUserProfile(ModifyUserMetaData userInfo)
        {

            var response = new ModifyActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    var thisUser = context.Users.SingleOrDefault(x => x.UserID == userInfo.UserID);
                    if (thisUser != null)
                    {
                        thisUser.EmailAddress = userInfo.EmailAddress;
                        thisUser.UserRole = Helpers.Encrypt(userInfo.UserRole);
                        thisUser.FirstName = userInfo.FirstName;
                        thisUser.LastName = userInfo.LastName;
                        thisUser.ModifiedOn = DateTime.Now;
                        thisUser.ModifiedBy = LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username;

                        context.SaveChanges();


                        response.IsSuccess = true;
                        response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                        Common.LogEvent(response.SuccessMessage + "|Modify User Profile-" + userInfo.UserID + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                    }


                    return response;

                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    throw;
                }
            }
        }
        /// <summary>
        /// Method updates login time
        /// </summary>
        /// <param name="username"></param>
        /// <param name="loginTime"></param>
        public void UpdateLoginTime(string username, DateTime loginTime)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspUpdateLoginTime(loginTime, username);
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                    
                }
            }
        }

        /// <summary>
        /// Method gets the current password of the user from the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>

        private string GetCurrentPassword(string username)
        {

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    string response = context.uspGetCurrentPassword(username).ToList().FirstOrDefault();
                    if (response != null || (string.IsNullOrEmpty(response)))
                    {
                        return response;
                    }
                    else
                    {
                        throw new Exception(Messages.GENERAL_ERROR);
                    }
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                    throw;

                }
            }
        }

        /// <summary>
        /// checks the current password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="formCurrentPassword"></param>
        /// <returns></returns>

        public bool IsCurrentPasswordValid(string username, string formCurrentPassword)
        {
            try
            {
                string currentPassword = GetCurrentPassword(username);
                return Helpers.HashPassword(formCurrentPassword).Equals(currentPassword);
            }
            catch (Exception ex)
            {
                Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                throw;
            }
        }

        /// <summary>
        /// Changes a user password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>

        public bool ChangeUserPassword(string username, string currentPassword, string newPassword)
        {
            // if the password is repeated
            if (currentPassword.Equals(newPassword))
            {
                throw new Exception(Messages.CURRENT_SAME_AS_NEW);
            }

            DateTime passwordExpiryDate = DateTime.Now.AddDays(Properties.Settings.Default.PasswordDurationDays);
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    context.uspChangePassword(Helpers.HashPassword(newPassword), passwordExpiryDate, username);
                    HttpContext.Current.Session["PasswordExpiryDate"] = Helpers.ConvertToFriendlyDate(passwordExpiryDate);
                    LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].HasPasswordExpired = false;
                    LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].FirstLogin = 0;
                   
                    return true;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method that creates a user profile
        /// </summary>
        /// <param name="profileInfo"></param>
        /// <returns></returns>
        public CreateUserResponseMetaData CreateUserProfile(UserMetaData profileInfo)
        {
            
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    var dbResponse = context.uspCreateUser(context.Users.Any() ? context.Users.Max(x => x.UserID) + 1 : 1,
                                        Helpers.SanitiseGeneratedUsername(profileInfo.Username),
                                        Common.textInfo.ToTitleCase(profileInfo.FirstName),
                                        Common.textInfo.ToTitleCase(profileInfo.LastName),
                                        profileInfo.EmailAddress,
                                        Helpers.HashPassword(profileInfo.UserPassword),
                                        Helpers.Encrypt(profileInfo.UserRole),
                                        profileInfo.CreatedOn,
                                        profileInfo.CreatedBy
                                     );
                    string generatedUsername = dbResponse.ToList().FirstOrDefault();
                    var response = new CreateUserResponseMetaData
                    {
                        Username = generatedUsername,
                        PossibleErrorMessage = string.Empty,

                    };
                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method checks if a user profile is locked
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public LockResponse IsProfileLocked(string username)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    var lockResponse = new LockResponse();
                    var response = context.uspIsProfileLocked(username).ToList().FirstOrDefault();
                    if (response != null)
                    {
                        lockResponse.UnlockTime = response.UnlockTime;
                        lockResponse.IsLocked = true;
                    }
                    else
                    {
                        lockResponse.IsLocked = false;
                        lockResponse.UnlockTime = null;
                    }
                    return lockResponse;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                    throw;
                }
            }
        }

        /// <summary>
        /// Method authenticates a user
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>

        public AuthenticationResponse? AuthenticateUserLogin(AuthenticationMetaData userLogin)
        {
            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {
                    var dbResponse = context.uspAuthenticateUser(userLogin.Username, Helpers.HashPassword(userLogin.Password), new DateTime(1990, 1, 1)).ToList().FirstOrDefault();
                    if (dbResponse != null)
                    {
                        AuthenticationResponse response = new AuthenticationResponse
                        {
                            Username = dbResponse.Username,
                            UserRole = dbResponse.UserRole,
                            UserID = dbResponse.UserID,
                            Active = Convert.ToInt32(dbResponse.Active),
                            FirstName = Common.textInfo.ToTitleCase(dbResponse.FirstName),
                            LastName = Common.textInfo.ToTitleCase(dbResponse.LastName),
                            FirstLogin = Convert.ToInt32(dbResponse.FirstLogin),
                            PasswordExpiryDate = dbResponse.PasswordExpiryDate,
                            EmailAddress = Helpers.IsNull(dbResponse.EmailAddress, string.Empty),
                            IsValidLogin = true,
                            HasPasswordExpired = this.HasPasswordExpired(dbResponse.PasswordExpiryDate.GetValueOrDefault()),
                            LastLogin = Helpers.IsNull(dbResponse.LastLogin.GetValueOrDefault(), DateTime.MinValue),
                            IsLocked = Helpers.IsNull(dbResponse.Locked, false),
                            UnlockTime = Convert.ToDateTime(dbResponse.UnlockTime),
                        };
                        return response;
                    }
                    else
                    {
                        var ThisResponse = new AuthenticationResponse
                        {
                            IsValidLogin = false,
                            PossibleErrorMessage = Messages.WRONG_CREDENTIALS,
                        };
                        return ThisResponse;
                    }
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|");
                    throw;
                }
            }
        }

        /// <summary>
        /// Method that checks if a password has expired
        /// </summary>
        /// <param name="passwordExpiryDate"></param>
        /// <returns></returns>
        private bool HasPasswordExpired(DateTime passwordExpiryDate)
        {
            if (passwordExpiryDate == new DateTime(1900, 1, 1) || passwordExpiryDate == new DateTime(1990, 1, 1))
            {
                return false;
            }
            return passwordExpiryDate < DateTime.Now ? true : false;
        }

        /// <summary>
        /// Method that deactivates (set Active=false) a user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ModifyActionResult DeactivateUser(int userID)
        {
            var response = new ModifyActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {

                    var user = context.Users.SingleOrDefault(x => x.UserID == userID);
                    if (user != null)
                    {
                        user.Active = false;
                        user.ModifiedOn = DateTime.Now;
                        user.ModifiedBy = LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username;
                    }

                    context.SaveChanges();


                    response.IsSuccess = true;
                    response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                    Common.LogEvent(response.SuccessMessage + "|Deactivate User-" + userID + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    throw;
                }
            }
        }


        /// <summary>
        /// Method that Activates (set Active=true) a user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ModifyActionResult ActivateUser(int userID)
        {
           
            var response = new ModifyActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {

                    var user = context.Users.SingleOrDefault(x => x.UserID == userID && x.Active == false);
                    if (user != null)
                    {
                        user.Active = true;
                        user.ModifiedOn = DateTime.Now;
                        user.ModifiedBy = LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username;
                    }

                    context.SaveChanges();


                    response.IsSuccess = true;
                    response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                    Common.LogEvent(response.SuccessMessage + "|Activate User-" + userID + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);

                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    throw;
                }
            }
        }


        /// <summary>
        /// Method that deletes a user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ModifyActionResult DeleteUser(int userID)
        {
            var response = new ModifyActionResult();

            using (Patient_ManagementEntities context = new Patient_ManagementEntities(DBConnect.MainConnectionString))
            {
                try
                {


                    var user = context.Users.SingleOrDefault(x => x.UserID == userID);
                    if (user != null)
                    {
                        context.Users.Remove(user);
                    }

                    context.SaveChanges();

                    response.IsSuccess = true;
                    response.SuccessMessage = Messages.OPERATION_COMPLETED_SUCCESSFULLY;
                    Common.LogEvent(response.SuccessMessage + "|Deactivate User-" + userID + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);


                    return response;
                }
                catch (Exception ex)
                {
                    Common.LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now + "|" + LoginCache[(UserKey)HttpContext.Current.Session["UserKey"]].Username);
                    throw;
                }
            }
        }
    }
}