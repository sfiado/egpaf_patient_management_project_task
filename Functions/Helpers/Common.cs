﻿using System;
using System.IO;
using System.Web;
using System.Globalization;
using System.Collections.Generic;
using System.Web.Mvc;
using static Patient_Management_Project.Helpers.SessionManager;
using System.Linq;
using Patient_Management_Project.Models.DAL;
using Patient_Management_Project.EF;

namespace Patient_Management_Project
{
    public static partial class Common
    {
        public static string logFilePath = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/User_Triggered_Event") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
        public static string securityLogFilePath = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/Unauthorized_Access_Attempt")+ "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
        public static string userAccessedPathLogFile = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/User_Accessed_System_Route") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
        public static string systemErrorLogFile = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/System_Error")+ "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
        public static TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        public struct FileMetaData
        {
            public Byte[] FileContent;
            public string FileExtension;
            public string FileName;
            public string FileOwner;
        }


        public static void GlobalVariableInitializer()
        {
            try
            {
               
                logFilePath = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/User_Triggered_Event") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
                securityLogFilePath = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/Unauthorized_Access_Attempt") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
                userAccessedPathLogFile = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/User_Accessed_System_Route") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
                systemErrorLogFile = HttpContext.Current.Server.MapPath("~/Patient_Management_Project_Logs/System_Error") + "\\" + $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
                
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Currency Select list
        /// </summary>

        /// <returns></returns>

        public static IEnumerable<SelectListItem> GetSystemRole()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Clerk", Value = "Clerk"},
                new SelectListItem{Text = "Administrator", Value = "Administrator"},
            };
            return items;
        }


        /// <summary>
        /// Method that logs user generated events
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static void LogEvent(string log)
        {
            GlobalVariableInitializer();
            if (!Directory.Exists(logFilePath))
            {
                Directory.CreateDirectory(logFilePath);
            }

            log = Helpers.Encrypt(log);

            string fileName = "Log_File";

            using (System.IO.StreamWriter file =
                                        new System.IO.StreamWriter(logFilePath + "/" + Helpers.Encrypt(fileName) + ".EGPAF", true))
            {
                file.WriteLine("**************************************************************");
                file.WriteLine(log);
            }
        }

        /// <summary>
        /// Method that logs unauthorised access attempts
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static void LogUnauthorizedAccessAttempt(string log)
        {
            GlobalVariableInitializer();
            if (!Directory.Exists(securityLogFilePath))
            {
                Directory.CreateDirectory(securityLogFilePath);
            }

            log = Helpers.Encrypt(log);

            string fileName = "Log_File";

            using (System.IO.StreamWriter file =
                                        new System.IO.StreamWriter(securityLogFilePath + "/" + Helpers.Encrypt(fileName) + ".EGPAF", true))
            {
                file.WriteLine("**************************************************************");
                file.WriteLine(log);
            }
        }


        /// <summary>
        /// Method that logs system user clicks
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static void LogUserSystemAccess(string log)
        {
            GlobalVariableInitializer();
            if (!Directory.Exists(userAccessedPathLogFile))
            {
                Directory.CreateDirectory(userAccessedPathLogFile);
            }

            log = Helpers.Encrypt(log);

            string fileName = "Log_File";

            using (System.IO.StreamWriter file =
                                        new System.IO.StreamWriter(userAccessedPathLogFile + "/" + Helpers.Encrypt(fileName) + ".EGPAF", true))
            {
                file.WriteLine("**************************************************************");
                file.WriteLine(log);
            }
        }

        /// <summary>
        /// Method that logs system errors emanating from error handling component
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public static void LogSystemError(string log)
        {
            GlobalVariableInitializer();
            try
            {

                if (!Directory.Exists(systemErrorLogFile))
                {
                    Directory.CreateDirectory(systemErrorLogFile);
                }

                log = Helpers.Encrypt(log);

                string fileName = "Log_File";

                using (System.IO.StreamWriter file =
                                            new System.IO.StreamWriter(systemErrorLogFile + "/" + Helpers.Encrypt(fileName) + ".EGPAF", true))
                {
                    file.WriteLine("**************************************************************");
                    file.WriteLine(log);
                }
            }

            catch (Exception ex)
            {
                LogSystemError(ex.GetBaseException().ToString() + "|" + DateTime.Now);
                throw;
            }


        }

        /// <summary>
        /// Gender Select list
        /// </summary>

        /// <returns></returns>

        public static IEnumerable<SelectListItem> GetGender()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Male", Value = "Male"},
                new SelectListItem{Text = "Female", Value = "Female"}
            };
            return items;
        }

        /// <summary>
        /// Gender Select list
        /// </summary>

        /// <returns></returns>

        public static IEnumerable<SelectListItem> GetIDTypes()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "National ID", Value = "National ID"},
                new SelectListItem{Text = "Passport", Value = "Passport"},
                new SelectListItem{Text = "Driving License", Value = "Driving License"}
            };
            return items;
        }

        /// <summary>
        /// Baptism Select list
        /// </summary>

        /// <returns></returns>

        public static IEnumerable<SelectListItem> GetOccupationList()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Software Developer", Value = "Software Developer"},
                new SelectListItem{Text = "Computer Scientist", Value = "Computer Sceintist"},
                new SelectListItem{Text = "Teacher", Value = "Teacher"},
                new SelectListItem{Text = "University Lecturer", Value = "University Lecturer"},
                new SelectListItem{Text = "Medial Doctor", Value = "Medical Doctor"},
                new SelectListItem{Text = "Dentist", Value = "Dentist"},
                new SelectListItem{Text = "Data Scientist", Value = "Data Sceintist"},
                new SelectListItem{Text = "M & E", Value = "M & E"},
                new SelectListItem{Text = "Entrepreneur", Value = "Entrepreneur"},
                new SelectListItem{Text = "Other", Value = "Other"}

            };
            return items;
        }

    }

}
