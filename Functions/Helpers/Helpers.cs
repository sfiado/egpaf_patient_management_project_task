﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Patient_Management_Project
{
    public static partial class Helpers
    {
        // The Initialization Vector for the DES encryption routine
        private static readonly byte[] IV = new byte[8] { 240, 3, 45, 29, 0, 76, 173, 59 };
        private const string cryptoKey = "EGPAF";
        public static readonly string NumericRegex = @"^\d+$";
        
        #region Password Helpers
        /// <summary>
        /// Checks if a string contains symbol
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        public static bool PasswordContainsSymbol(string value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                if (!char.IsLetterOrDigit(value[i]))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if a string contains a number
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        public static bool PasswordContainsNumber(string value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsDigit(value[i]))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if a string contains an upper case letter
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// 
        public static bool PasswordContainsUppercase(string value)
        {
            // Consider string to be uppercase if it has no lowercase letters.
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsUpper(value[i]))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Hashes a plain text password
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>

        public static string HashPassword(string plainText)
        {
            try
            {
                SHA512Managed managed = new SHA512Managed();
                return Convert.ToBase64String(managed.ComputeHash(Encoding.ASCII.GetBytes(plainText)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static T IsNull<T>(T Value, T DefaultValue)
        {
            if (Value == null || Convert.IsDBNull(Value))
            {
                return DefaultValue;
            }
            else
            {
                return Value;
            }
        }

        /// <summary>
        /// Decrypts provided string parameter
        /// </summary>
        ///
        public static string Decrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;
            string result = string.Empty;
            try
            {
                byte[] buffer = Convert.FromBase64String(s);
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                des.Key = MD5.ComputeHash(Encoding.ASCII.GetBytes(cryptoKey));
                des.IV = IV;
                result = Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Encrypts provided string parameter
        /// </summary>
        public static string Encrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;
            string result = string.Empty;
            try
            {
                byte[] buffer = Encoding.ASCII.GetBytes(s);
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));
                des.IV = IV;
                result = Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Method gets the IP Address of a machine
        /// </summary>
        /// <returns></returns>

        public static string GetIPAddress()
        {
            return System.Web.HttpContext.Current.Request.UserHostAddress;
        }

        /// <summary>
        /// Sanitises input by removing invalid characters and trimming spaces
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>

        public static string SanitiseInput(string inputString)
        {
            try
            {
                return Regex.Replace(inputString, "[^.A-Za-z0-9 _]", string.Empty).Trim();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// sanitize the auto generated username
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 

        public static string SanitiseGeneratedUsername(string input)
        {
            return Regex.Replace(input, "[^a-zA-Z]", string.Empty);
        }
        /// <summary>
        /// Removes whitespaces from a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        public static string RemoveWhitespace(string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        /// <summary>
        /// Method converts a .NET date to a friendly date
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>

        public static string ConvertToFriendlyDate(DateTime Input, bool convertToShortDate = false)
        {
            try
            {
                if (convertToShortDate)
                {
                    return Input.ToString("dd MMM yyyy");
                }
                return Input.ToString("dd MMM yyyy HH:mm");
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// For any given date, converts it to HH:59:59 e.g. 11:59:59
        /// </summary>
        /// <returns></returns>
        public static DateTime AppendTime(DateTime Input)
        {
            return Input.AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        /// <summary>
        /// For any given date, converts it to HH:00:00 e.g. 00:00:00
        /// </summary>
        /// <returns></returns>
        public static DateTime TruncateTime(DateTime Input)
        {
            return Input.AddHours(23).AddMinutes(59).AddSeconds(59);
        }

       
    }
}