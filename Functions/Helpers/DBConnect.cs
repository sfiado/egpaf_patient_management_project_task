﻿
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace Patient_Management_Project
{
    public static class DBConnect
    {
        private static string BuildConnectionString()
        {
            SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder
            {
                DataSource = Helpers.Decrypt(Properties.Settings.Default.DataSource),
                InitialCatalog = Helpers.Decrypt(Properties.Settings.Default.InitialCatalog),
                UserID = Helpers.Decrypt(Properties.Settings.Default.UserID),
                Password = Helpers.Decrypt(Properties.Settings.Default.Password),
            };
            return connection.ConnectionString;
        }

        /// <summary>
        /// Builds an SQL server connection string
        /// </summary>
        /// <returns></returns>
       
        private static EntityConnectionStringBuilder EGPAFTask = new EntityConnectionStringBuilder()
        {
            Provider = "System.Data.SqlClient",
            Metadata = "res://*/EF.Patient_Management.csdl|res://*/EF.Patient_Management.ssdl|res://*/EF.Patient_Management.msl",
            ProviderConnectionString = BuildConnectionString(),
        };


        public static string EGPAFTaskConnectionString
        {
            get
            {
                return EGPAFTask.ConnectionString;
            }
        }

        public static string MainConnectionString
        {
            get
            {
                return EGPAFTask.ConnectionString;
            }
        }
    }
}