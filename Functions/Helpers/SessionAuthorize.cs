﻿using System.Linq;
using System.Web.Mvc;
using System;
using static Patient_Management_Project.Helpers.SessionManager;
using Patient_Management_Project.Functions;

namespace Patient_Management_Project
{
    public class SessionAuthorize : AuthorizeAttribute
    {
        private UserKey userKey;
        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            // this user is probably not logged on
            if (context.HttpContext.Session["UserKey"] == null)
            {
                context.Result = new RedirectResult($"/{"Home"}/{"Index"}");
                
            }
            else
            {
                // check the password validity and permissions...
                userKey = (UserKey)context.HttpContext.Session["UserKey"];
                var userVals = LoginCache[userKey];
                var User = LoginCache[userKey];
                if (User.HasPasswordExpired)
                {
                    context.Result = new RedirectResult($"/{"Home"}/{"ChangePassword"}");
                    
                    RenewSession(userKey);
                }
                else if (User.FirstLogin > 0)
                {
                    context.Result = new RedirectResult($"/{"Home"}/{"ChangePassword"}");
                    
                    RenewSession(userKey);
                }
                else if (IsSessionValid(userKey))
                {

                    var destination = context.HttpContext.Request.Path;
                    string message = string.Empty;
                    message = string.Format(Messages.SYSTEM_ACCESS, context.HttpContext.Session["Username"], destination, DateTime.Now);
                    Common.LogUserSystemAccess(message);

                    RenewSession(userKey);


                }
                else
                {
                    context.Result = new RedirectResult($"/{"Home"}/{"Index"}");
                }
            }
        }
    }
}