﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Patient_Management_Project
{
    static partial class Helpers
    {
        public static class SessionManager
        {
            public static ConcurrentDictionary<UserKey, UserInfo> LoginCache = new ConcurrentDictionary<UserKey, UserInfo>();
            public static ConcurrentDictionary<string, Int32> LoginAttemptsCache = new ConcurrentDictionary<string, int>();
            
            public static List<string> AdministratorRoutes = new List<string>();
           
            public struct UserKey
            {
                public string Username;
                public string IPAddress;
                public Guid Token;
                public string UserAgent;
            }

            public class UserInfo
            {
                public string Username { get; set; }
                public int UserID { get; set; }
                public Guid UniqueToken { get; set; }
                public string UserRole { get; set; }
                public DateTime TokenExpiryTime { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string ConnString { get; set; }
                public string ConnStringClient { get; set; }
                public bool HasPasswordExpired { get; set; }
                public int FirstLogin { get; set; }
                public string EmailAddress { get; set; }

            }

            public static void InitAdministratorRoutes()
            {
                AdministratorRoutes.Clear();

                AdministratorRoutes.Add($"/{"User"}/{"ManageUser"}");
                AdministratorRoutes.Add($"/{"User"}/{"NewUser"}");
                AdministratorRoutes.Add($"/{"User"}/{"ModifyUser"}");
                AdministratorRoutes.Add($"/{"User"}/{"ResetPassword"}");
                AdministratorRoutes.Add($"/{"User"}/{"DeactivateUser"}");
                AdministratorRoutes.Add($"/{"User"}/{"ActivateUser"}");
                AdministratorRoutes.Add($"/{"User"}/{"DeleteUser"}");

                AdministratorRoutes.Add($"/{"Patient"}/{"DeactivatePatient"}");
                AdministratorRoutes.Add($"/{"Patient"}/{"ActivatePatient"}");
                AdministratorRoutes.Add($"/{"Patient"}/{"DeletePatient"}");

                AdministratorRoutes.Add($"/{"Patient"}/{"DeleteMedicalRecord"}");
            }

            /// <summary>
            /// Initialize system routes
            /// </summary>

            public static void InitializeSystemRoutes()
            {
                try
                {
                    InitAdministratorRoutes();
                    
                }
                catch (Exception) { }
            }

            /// <summary>
            /// Removes expired sessions
            /// </summary>

            public static void AutoKillExpiredSessions()
            {
                try
                {
                    IEnumerable<KeyValuePair<UserKey, UserInfo>> Sessions = LoginCache.Where(x => x.Value.TokenExpiryTime < DateTime.Now);
                    foreach (KeyValuePair<UserKey, UserInfo> item in Sessions)
                    {
                        // remove expired sessions
                        KillSession(item.Key);
                    }
                }
                catch (Exception) { }
            }

            /// <summary>
            /// Registers a new login session
            /// </summary>
            /// <param name="userID"></param>
            /// <param name="userDetails"></param>

            public static void RegisterNewSession(UserKey userID, UserInfo userDetails)
            {
                try
                {
                    // this is a session hack
                    if (LoginCache.ContainsKey(userID))
                    {
                        throw new DuplicateNameException($"A session with token key {userID.Token} {userID.UserAgent} exists");
                    }
                    LoginCache.TryAdd(userID, userDetails);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            /// <summary>
            /// Renews a continuing session
            /// </summary>
            /// <param name="userID"></param>

            public static void RenewSession(UserKey userID)
            {
                try
                {
                    var Value = new UserInfo();
                    LoginCache.TryRemove(userID, out Value);
                    Value.TokenExpiryTime = DateTime.Now.AddMinutes(Properties.Settings.Default.AutoKillTime);
                    LoginCache.TryAdd(userID, Value);
                }
                catch (Exception) { }
            }

            /// <summary>
            /// Kills a session on log-out or timeout
            /// </summary>
            /// <param name="userID"></param>

            public static void KillSession(UserKey userID)
            {
                try
                {
                    var Value = new UserInfo();
                    LoginCache.TryRemove(userID, out Value);
                }
                catch (Exception) { }
            }

            /// <summary>
            /// Checks if a session is valid
            /// </summary>
            /// <param name="userID"></param>
            /// <returns></returns>

            public static bool IsSessionValid(UserKey userID)
            {
                try
                {
                    if (!LoginCache.ContainsKey(userID))
                    {
                        return false;
                    }
                    else
                    {
                        var item = LoginCache[userID];
                        if (DateTime.Now > item.TokenExpiryTime)
                        {
                            return false;
                        }
                        else if (!userID.Token.Equals(item.UniqueToken))
                        {
                            // to prevent session hijacks...
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}