﻿namespace Patient_Management_Project.Functions
{
    public static class Messages
    {

        public static readonly string SYSTEM_ACCESS = "User {0} accessed {1} on {2}";
        public static readonly string UNAUTHORIZED_ACCESS = "User {0} tried to Access {1} on {2}";
        public static readonly string USER_CREATED = "User profile created. The username is {0}";
        public static readonly string USER_REGISTERED_SUCCESSFULLY = "Self registration successful. Profile has been created and your username is {0}. Please login";
        public static readonly string USER_PROFILE_DELETED = "The user profile {0} has been deleted from the system";
        public static readonly string PASSWORD_RESET = "The password has been reset for the user {0}";
        public static readonly string PROFILE_UPDATED = "The user profile has successfully been updated";
        public static readonly string SELECT_USER_STATUS = "Please select a status for the user";
        public static readonly string CURRENT_PASSWORD_INCORRECT = "The current password was not confirmed correctly. Please try again";
        public static readonly string PASSWORD_CHANGED = "The password has been changed successfully, Click on the Dashboard";
        public static readonly string POLICY_NOT_MET = "Your password does not meet the complexity requirements: {0}";
        public static readonly string UNAUTHORISED_ACCESS = "You do not have authorisation to access the intended resource";
        public static readonly string FIRST_LOGIN = "You are required to change your password at first login";
        public static readonly string PROFILE_INACTIVE = "Profile inactive. Contact helpdesk";
        public static readonly string LOCKOUT = "Too many invalid attempts. Try again in {0} {1}";
        public static readonly string NO_USERS = "There are no users in the system";
        public static readonly string WRONG_CREDENTIALS = "Wrong username or password entered";
        public static readonly string CURRENT_SAME_AS_NEW = "The current password is the same as the new password";
        public static readonly string PASSWORD_EXPIRED = "Your password has expired and must be changed";
        public static readonly string OPERATION_COMPLETED_SUCCESSFULLY = "Operation completed successfully";
        public static readonly string GENERAL_ERROR = "Something went wrong. Try again later";
        public static readonly string SELECT_USER_ROLE = "Select User Role";
        public static readonly string INVALID_DATE = "Sorry, the date you have put is invalid";

    }
}