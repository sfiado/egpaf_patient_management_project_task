USE [master]
GO
/****** Object:  Database [PATIENT_MANAGEMENT_SYSTEM]    Script Date: 10-Feb-21 12:40:23 PM ******/
CREATE DATABASE [PATIENT_MANAGEMENT_SYSTEM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PATIENT_MANAGEMENT_SYSTEM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PATIENT_MANAGEMENT_SYSTEM.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PATIENT_MANAGEMENT_SYSTEM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PATIENT_MANAGEMENT_SYSTEM_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PATIENT_MANAGEMENT_SYSTEM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ARITHABORT OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET RECOVERY FULL 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET  MULTI_USER 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PATIENT_MANAGEMENT_SYSTEM', N'ON'
GO
USE [PATIENT_MANAGEMENT_SYSTEM]
GO
/****** Object:  Table [dbo].[DiagnosisCodes]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiagnosisCodes](
	[DiagnosisCodeID] [int] NOT NULL,
	[DiagnosisCodeName] [varchar](50) NOT NULL,
	[DiagnosisCodeDescription] [text] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisCodes_CreatedOn]  DEFAULT (getdate()),
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_DiagnosisCodes_DiagnosisCodeID] PRIMARY KEY CLUSTERED 
(
	[DiagnosisCodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoginActivity]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoginActivity](
	[AutoID] [bigint] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[IPAddress] [varchar](50) NOT NULL,
	[IsSuccess] [bit] NOT NULL CONSTRAINT [DF_LoginActivity_IsSuccess]  DEFAULT ((0)),
	[DateLogged] [datetime] NOT NULL,
 CONSTRAINT [PK_LoginActivity] PRIMARY KEY CLUSTERED 
(
	[AutoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientDiagnosis]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientDiagnosis](
	[DiagnosisID] [int] NOT NULL,
	[DiagnosisCodeID] [int] NOT NULL,
	[VisitID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_PatientDiagnosis] PRIMARY KEY CLUSTERED 
(
	[DiagnosisID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Patients](
	[PatientAutoID] [int] NOT NULL,
	[PatientIDNumber] [varchar](50) NOT NULL,
	[PatientIDType] [varchar](50) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Gender] [varchar](6) NOT NULL,
	[Birthdate] [date] NOT NULL,
	[CurrentAddress] [text] NOT NULL,
	[Occupation] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Patients_PatientAutoID] PRIMARY KEY CLUSTERED 
(
	[PatientAutoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientVisits]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientVisits](
	[VisitID] [int] NOT NULL,
	[VisitDate] [date] NOT NULL,
	[PatientAutoID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_PatientVisits_VisitID] PRIMARY KEY CLUSTERED 
(
	[VisitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientVitals]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientVitals](
	[VitalsID] [int] NOT NULL,
	[VisitID] [int] NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[Height] [decimal](18, 2) NOT NULL,
	[Temperature] [decimal](18, 2) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_PatientVitals_VitalsID] PRIMARY KEY CLUSTERED 
(
	[VitalsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NULL,
	[EmailAddress] [varchar](80) NOT NULL,
	[Password] [varchar](500) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserRole] [varchar](30) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [varchar](30) NOT NULL,
	[LastLogin] [datetime] NULL,
	[PasswordExpiryDate] [datetime] NULL,
	[FirstLogin] [bit] NULL,
	[Locked] [bit] NULL,
	[UnlockTime] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vw_PatientHealthRecord]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_PatientHealthRecord]
AS
SELECT        dbo.PatientVisits.VisitID, dbo.PatientVisits.VisitDate, dbo.PatientVisits.PatientAutoID, dbo.PatientVisits.CreatedBy, dbo.PatientVisits.CreatedOn, dbo.PatientVisits.ModifiedBy, dbo.PatientVisits.ModifiedOn, 
                         dbo.PatientVitals.VitalsID, dbo.PatientVitals.Weight, dbo.PatientVitals.Height, dbo.PatientVitals.Temperature, dbo.PatientDiagnosis.DiagnosisID, dbo.PatientDiagnosis.DiagnosisCodeID, 
                         dbo.DiagnosisCodes.DiagnosisCodeName, dbo.DiagnosisCodes.DiagnosisCodeDescription, dbo.Patients.PatientIDNumber, dbo.Patients.PatientIDType, dbo.Patients.FirstName, dbo.Patients.LastName, dbo.Patients.Gender, 
                         dbo.Patients.Birthdate, dbo.Patients.CurrentAddress, dbo.Patients.Occupation
FROM            dbo.PatientVisits INNER JOIN
                         dbo.PatientVitals ON dbo.PatientVisits.VisitID = dbo.PatientVitals.VisitID INNER JOIN
                         dbo.PatientDiagnosis ON dbo.PatientVisits.VisitID = dbo.PatientDiagnosis.VisitID INNER JOIN
                         dbo.DiagnosisCodes ON dbo.PatientDiagnosis.DiagnosisCodeID = dbo.DiagnosisCodes.DiagnosisCodeID INNER JOIN
                         dbo.Patients ON dbo.PatientVisits.PatientAutoID = dbo.Patients.PatientAutoID

GO
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'A00-B99', N'Certain infectious and parasitic diseases', N'sfiado', CAST(N'2021-02-09 10:49:26.050' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'C00-D49', N' Neoplasms
', N'sfiado', CAST(N'2021-02-09 10:50:21.640' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'D50-D89', N' Diseases of the blood and blood-forming organs and certain disorders involving the immune mechanism
', N'sfiado', CAST(N'2021-02-09 10:51:02.870' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'E00-E89', N' Endocrine, nutritional and metabolic diseases
', N'sfiado', CAST(N'2021-02-09 10:51:38.037' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, N'F01-F99 
', N' Mental, Behavioral and Neurodevelopmental disorders
', N'sfiado', CAST(N'2021-02-09 10:51:57.703' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, N'G00-G99', N' Diseases of the nervous system
', N'sfiado', CAST(N'2021-02-09 10:52:23.947' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, N'H00-H59', N' Diseases of the eye and adnexa
', N'sfiado', CAST(N'2021-02-09 10:52:46.417' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (8, N'H60-H95', N' Diseases of the ear and mastoid process
', N'sfiado', CAST(N'2021-02-09 10:53:15.660' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (9, N'I00-I99', N' Diseases of the circulatory system
', N'sfiado', CAST(N'2021-02-09 10:53:56.130' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (10, N'J00-J99', N' Diseases of the respiratory system
', N'sfiado', CAST(N'2021-02-09 10:54:14.833' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (11, N'K00-K95', N' Diseases of the digestive system
', N'sfiado', CAST(N'2021-02-09 10:54:34.990' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (12, N'L00-L99', N' Diseases of the skin and subcutaneous tissue
', N'sfiado', CAST(N'2021-02-09 10:55:05.533' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (13, N'M00-M99', N' Diseases of the musculoskeletal system and connective tissue
', N'sfiado', CAST(N'2021-02-09 10:55:44.253' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (14, N'N00-N99', N' Diseases of the genitourinary system
', N'sfiado', CAST(N'2021-02-09 10:56:16.723' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (15, N'O00-O9A', N' Pregnancy, childbirth and the puerperium
', N'sfiado', CAST(N'2021-02-09 10:56:42.583' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (16, N'P00-P96', N' Certain conditions originating in the perinatal period
', N'sfiado', CAST(N'2021-02-09 10:57:03.100' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (17, N'Q00-Q99 ', N' Congenital malformations, deformations and chromosomal abnormalities
', N'sfiado', CAST(N'2021-02-09 10:57:23.143' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (18, N'R00-R99', N' Symptoms, signs and abnormal clinical and laboratory findings, not elsewhere classified
', N'sfiado', CAST(N'2021-02-09 10:57:49.537' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (19, N'S00-T88', N' Injury, poisoning and certain other consequences of external causes
', N'sfiado', CAST(N'2021-02-09 10:58:33.397' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (20, N'U00-U85', N' Codes for special purposes
', N'sfiado', CAST(N'2021-02-09 10:59:03.527' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (21, N'V00-Y99', N' External causes of morbidity
', N'sfiado', CAST(N'2021-02-09 10:59:30.503' AS DateTime), NULL, NULL)
INSERT [dbo].[DiagnosisCodes] ([DiagnosisCodeID], [DiagnosisCodeName], [DiagnosisCodeDescription], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (22, N'Z00-Z99', N' Factors influencing health status and contact with health services
', N'sfiado', CAST(N'2021-02-09 10:59:56.310' AS DateTime), NULL, NULL)
INSERT [dbo].[LoginActivity] ([AutoID], [Username], [IPAddress], [IsSuccess], [DateLogged]) VALUES (1, N'sfiado', N'::1', 0, CAST(N'2021-02-10 12:25:57.733' AS DateTime))
INSERT [dbo].[LoginActivity] ([AutoID], [Username], [IPAddress], [IsSuccess], [DateLogged]) VALUES (2, N'sfiado', N'::1', 1, CAST(N'2021-02-10 12:26:03.447' AS DateTime))
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DiagnosisCodes_DiagnosisCodeName]    Script Date: 10-Feb-21 12:40:23 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DiagnosisCodes_DiagnosisCodeName] ON [dbo].[DiagnosisCodes]
(
	[DiagnosisCodeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Patients_PatientIDNumber]    Script Date: 10-Feb-21 12:40:23 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Patients_PatientIDNumber] ON [dbo].[Patients]
(
	[PatientIDNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientDiagnosis] ADD  CONSTRAINT [DF_PatientDiagnosis_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PatientVisits] ADD  CONSTRAINT [DF_PatientVisits_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PatientVitals] ADD  CONSTRAINT [DF_PatientVitals_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_FirstLogin]  DEFAULT ((0)) FOR [FirstLogin]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Locked]  DEFAULT ((0)) FOR [Locked]
GO
ALTER TABLE [dbo].[PatientDiagnosis]  WITH CHECK ADD  CONSTRAINT [FK_PatientDiagnosis_DiagnosisCodes] FOREIGN KEY([DiagnosisCodeID])
REFERENCES [dbo].[DiagnosisCodes] ([DiagnosisCodeID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] CHECK CONSTRAINT [FK_PatientDiagnosis_DiagnosisCodes]
GO
ALTER TABLE [dbo].[PatientDiagnosis]  WITH CHECK ADD  CONSTRAINT [FK_PatientDiagnosis_PatientVisits] FOREIGN KEY([VisitID])
REFERENCES [dbo].[PatientVisits] ([VisitID])
GO
ALTER TABLE [dbo].[PatientDiagnosis] CHECK CONSTRAINT [FK_PatientDiagnosis_PatientVisits]
GO
ALTER TABLE [dbo].[PatientDiagnosis]  WITH CHECK ADD  CONSTRAINT [FK_PatientDiagnosis_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([Username])
GO
ALTER TABLE [dbo].[PatientDiagnosis] CHECK CONSTRAINT [FK_PatientDiagnosis_Users]
GO
ALTER TABLE [dbo].[Patients]  WITH CHECK ADD  CONSTRAINT [FK_Patients_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([Username])
GO
ALTER TABLE [dbo].[Patients] CHECK CONSTRAINT [FK_Patients_Users]
GO
ALTER TABLE [dbo].[PatientVisits]  WITH CHECK ADD  CONSTRAINT [FK_PatientVisits_Patients] FOREIGN KEY([PatientAutoID])
REFERENCES [dbo].[Patients] ([PatientAutoID])
GO
ALTER TABLE [dbo].[PatientVisits] CHECK CONSTRAINT [FK_PatientVisits_Patients]
GO
ALTER TABLE [dbo].[PatientVisits]  WITH CHECK ADD  CONSTRAINT [FK_PatientVisits_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([Username])
GO
ALTER TABLE [dbo].[PatientVisits] CHECK CONSTRAINT [FK_PatientVisits_Users]
GO
ALTER TABLE [dbo].[PatientVitals]  WITH CHECK ADD  CONSTRAINT [FK_PatientVitals_PatientVisits] FOREIGN KEY([VisitID])
REFERENCES [dbo].[PatientVisits] ([VisitID])
GO
ALTER TABLE [dbo].[PatientVitals] CHECK CONSTRAINT [FK_PatientVitals_PatientVisits]
GO
ALTER TABLE [dbo].[PatientVitals]  WITH CHECK ADD  CONSTRAINT [FK_PatientVitals_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([Username])
GO
ALTER TABLE [dbo].[PatientVitals] CHECK CONSTRAINT [FK_PatientVitals_Users]
GO
/****** Object:  StoredProcedure [dbo].[uspAuthenticateUser]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 08:59 am
-- Description:	Stored procedure to authenticate
--				a user
-- =============================================
CREATE PROCEDURE [dbo].[uspAuthenticateUser] 
	@Username varchar(30),
	@Password varchar(255),
	@minDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Username, UserRole, UserID, Active, FirstName, LastName, 
	ISNULL(FirstLogin,1) AS FirstLogin,
	ISNULL(PasswordExpiryDate, @minDate) PasswordExpiryDate, EmailAddress, 
	ISNULL(LastLogin, @minDate) AS LastLogin,
	ISNULL(Locked,0) AS Locked, 
	ISNULL(UnlockTime, @minDate) AS UnlockTime
	FROM Users
	WHERE ([UserName] = @Username) AND ([Password] = @Password)

END








GO
/****** Object:  StoredProcedure [dbo].[uspChangePassword]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 9:06 AM
-- Description:	Stored procedure to change 
--				a users password
-- =============================================
CREATE PROCEDURE [dbo].[uspChangePassword]
	@Password varchar(500),
	@PasswordExpiryDate datetime,
	@Username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Users] 
	SET [Password]       = @Password, 
	[FirstLogin]         = 0, 
	[PasswordExpiryDate] = @PasswordExpiryDate 
	WHERE [Username]     = @Username

END











GO
/****** Object:  StoredProcedure [dbo].[uspCreateUser]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 9:16 am
-- Description:	creates a user 
-- =============================================
CREATE PROCEDURE [dbo].[uspCreateUser]
	@UserID int,
	@Username varchar(50),
	@FirstName varchar(30),
	@LastName varchar(30),
	@EmailAddress varchar(255),
	@Password varchar(255),
	@UserRole varchar(30),
	@DateCreated datetime,
	@CreatedBy varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DoesUserExist int = 0
	DECLARE @Counter int = 0
	SELECT @DoesUserExist = COUNT(*) FROM [Users] WHERE [Username] = @Username

	WHILE @DoesUserExist > 0
		BEGIN
			SET @Counter = @Counter + 1
			SET @Username = @Username + '_' + CONVERT(VARCHAR,ROUND(RAND() * 100,0))
			SELECT @DoesUserExist = COUNT(*) FROM [Users] WHERE [Username] = @Username
			IF @DoesUserExist <= 0
				BREAK
			IF @Counter >= 5
				RAISERROR('Failed to create a user. Try again later',16,1)
				RETURN
		END

	INSERT INTO Users(UserID,Username, Firstname, LastName, EmailAddress, [Password], Active, UserRole, 
				DateCreated, CreatedBy, PasswordExpiryDate, FirstLogin)
	VALUES(@UserID,LOWER(@Username), @FirstName, @LastName, LOWER(@EmailAddress), @Password, 1, @UserRole, @DateCreated, @CreatedBy, NULL, 1)

	SELECT LOWER(@Username) AS Username

END












GO
/****** Object:  StoredProcedure [dbo].[uspDeleteUser]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspDeleteUser] 
	@DeletedBy varchar(30),
	@DeletedUser varchar(30),
	@DeletedTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements
	SET NOCOUNT ON;

	DELETE FROM [Users] WHERE [Username] = @DeletedUser

	INSERT INTO DeletedUsers (DeletedUser, DeletedTime, DeletedBy)
	VALUES (@DeletedUser, @DeletedTime, @DeletedBy)

END










GO
/****** Object:  StoredProcedure [dbo].[uspGetCurrentPassword]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 09:22 am
-- Description:	Stored procedure to get the current vendor password
-- =============================================
CREATE PROCEDURE [dbo].[uspGetCurrentPassword]
	@Username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [Password] FROM [Users] WHERE [Username] = @Username

END












GO
/****** Object:  StoredProcedure [dbo].[uspGetUsers]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 09:26 am
-- Description:	Stored procedure gets all
--				users on the system
-- =============================================
CREATE PROCEDURE [dbo].[uspGetUsers] 
	@UserID numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [UserID]
      ,[Username]
      ,[FirstName]
      ,[LastName]
      ,[EmailAddress]
      ,[Password]
      ,Active
      ,[UserRole]
      ,[DateCreated]
      ,[CreatedBy]
	  ,CONVERT(VARCHAR(17), [LastLogin], 113) AS [LastLogin]
      ,[PasswordExpiryDate]
      ,[FirstLogin]
	FROM [dbo].[Users]
	WHERE [UserID] <> @UserID

END











GO
/****** Object:  StoredProcedure [dbo].[uspIsProfileLocked]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 09:27 am
-- Description:	Stored procedure to check if 
--				a profile is locked
-- =============================================
create PROCEDURE [dbo].[uspIsProfileLocked]
	@username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ISNULL(UnlockTime, '1900-01-01') AS UnlockTime, Locked
	FROM [dbo].[Users]
	WHERE Username = @username
	AND Locked > 0
END









GO
/****** Object:  StoredProcedure [dbo].[uspLockProfile]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samson Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 09:28 am
-- Description:	Stored procedure to lock a profile
-- =============================================
Create PROCEDURE [dbo].[uspLockProfile]
	@username varchar(50),
	@unlockTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Users]
	SET [Locked]    = 1,
	[UnlockTime]    = @unlockTime
	WHERE Username  = @username

END








GO
/****** Object:  StoredProcedure [dbo].[uspLogLoginActivity]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspLogLoginActivity]
	@AutoID bigint,
	@Username varchar(50),
	@IPAddress varchar(50),
	@IsSuccess bit,
	@DateLogged datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO LoginActivity (AutoID,Username, IPAddress, IsSuccess, DateLogged)
	VALUES (@AutoID,@Username, @IPAddress, @IsSuccess, @DateLogged)

END









GO
/****** Object:  StoredProcedure [dbo].[uspUnlockProfile]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Samsoon Fiado <sfiadoh@gmail.com>
-- Create date: 09/02/2021 09:30 am
-- Description:	Stored procedure to unlock a profile
-- =============================================
CREATE PROCEDURE [dbo].[uspUnlockProfile]
	@username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Users]
	SET [Locked]    = 0,
	[UnlockTime]    = NULL
	WHERE Username  = @username

END









GO
/****** Object:  StoredProcedure [dbo].[uspUpdateLoginTime]    Script Date: 10-Feb-21 12:40:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[uspUpdateLoginTime]
	@LoginTime datetime,
	@Username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Users 
	SET LastLogin  = @LoginTime 
	WHERE Username = @Username 

END











GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[57] 4[12] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PatientVisits"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PatientVitals"
            Begin Extent = 
               Top = 35
               Left = 294
               Bottom = 165
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "PatientDiagnosis"
            Begin Extent = 
               Top = 25
               Left = 565
               Bottom = 155
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DiagnosisCodes"
            Begin Extent = 
               Top = 146
               Left = 31
               Bottom = 276
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Patients"
            Begin Extent = 
               Top = 148
               Left = 450
               Bottom = 278
               Right = 631
            End
            DisplayFlags = 280
            TopColumn = 8
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PatientHealthRecord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PatientHealthRecord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PatientHealthRecord'
GO
USE [master]
GO
ALTER DATABASE [PATIENT_MANAGEMENT_SYSTEM] SET  READ_WRITE 
GO
