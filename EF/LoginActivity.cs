//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patient_Management_Project.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class LoginActivity
    {
        public long AutoID { get; set; }
        public string Username { get; set; }
        public string IPAddress { get; set; }
        public bool IsSuccess { get; set; }
        public System.DateTime DateLogged { get; set; }
    }
}
