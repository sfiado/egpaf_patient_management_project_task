//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Patient_Management_Project.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class DiagnosisCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DiagnosisCode()
        {
            this.PatientDiagnosis = new HashSet<PatientDiagnosi>();
        }
    
        public int DiagnosisCodeID { get; set; }
        public string DiagnosisCodeName { get; set; }
        public string DiagnosisCodeDescription { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientDiagnosi> PatientDiagnosis { get; set; }
    }
}
