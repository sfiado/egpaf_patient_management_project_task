﻿using System.Web.Optimization;

namespace Patient_Management_Project
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/myscripts").Include(
                      "~/assets/scripts/jquery.min.js",
                      "~/assets/scripts/modernizr.min.js",
                      "~/assets/plugin/bootstrap/js/bootstrap.min.js",
                      "~/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js",
                      "~/assets/plugin/nprogress/nprogress.js",
                      "~/assets/plugin/sweet-alert/sweetalert.min.js",
                      "~/assets/plugin/waves/waves.min.js",
                      "~/assets/plugin/fullscreen/jquery.fullscreen-min.js",
                      "~/assets/scripts/main.min.js",
                      "~/assets/scripts/horizontal-menu.min.js",
                      "~/Scripts/datatables/jquery.dataTables.min.js",
                      "~/assets/scripts/multiselect.js",
                       "~/Scripts/datatables/dataTables.bootstrap.min.js",
                       "~/Scripts/chosen/chosen.jquery.min.js",
                       "~/Scripts/datatables/dataTables.bootstrap.min.js",
                       "~/Scripts/DataTables/dataTables.buttons.min.js",
                       "~/Scripts/DataTables/pdfmake.min.js",
                       "~/Scripts/DataTables/buttons.html5.min.js",
                       "~/Scripts/DataTables/buttons.print.min.js",
                       "~/Scripts/DataTables/jszip.min.js",
                      "~/assets/color-switcher/color-switcher.min.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/w3.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/mycss").Include(
                      "~/assets/styles/style-horizontal.css",
                      "~/assets/styles/style-horizontal.min.css",
                      //"~/assets/styles/main.css",
                      "~/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css",
                      "~/assets/plugin/waves/waves.min.css",
                      "~/assets/plugin/sweet-alert/sweetalert.css","" +
                      "~/assets/plugin/datatables/media/css/jquery.dataTables.min.css",
                      "~/content/bootstrap-datetimepicker.min.css",
                      "~/Content/multipleSelect.css",
                      "~/Content/multipleSelect.css",
                      "~/Content/chosen.min.css",
                      "~/content/datatables/css/datatables.bootstrap.css",
                      "~/assets/color-switcher/color-switcher.min.css"));
           
        }
    }
}
