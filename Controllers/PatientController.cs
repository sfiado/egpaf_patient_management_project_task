﻿using System;
using System.Linq;
using System.Web.Mvc;
using Patient_Management_Project.Functions;
using Patient_Management_Project.Models;
using Patient_Management_Project.Models.DAL;
using System.Collections.Generic;

namespace Patient_Management_Project.Controllers
{
    public class PatientController : Controller
    {
        
        [SessionAuthorize]
        public ActionResult ManagePatient()
        {
           
            try
            {
                IEnumerable<ViewPatient> patients = new PatientBL().GetRegisteredPatient();

                if (patients.Count() > 0)
                {
                    ViewBag.Patients = patients.ToList();
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;

            }
            return View();
        }

        [HttpPost]
        [SessionAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult NewPatient(ViewPatient obj)
        {
            try
            {

                var newPatient= new PatientBL.PatientMetaData
                {

                    FirstName = Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.FirstName)),
                    LastName = Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.LastName)),
                    PatientIDNumber=obj.PatientIDNumber,
                    PatientIDType=obj.PatientIDType,
                    Gender=obj.Gender,
                    BirthDate=obj.BirthDate,
                    CurrentAddress= Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.CurrentAddress)),
                    Occupation=obj.Occupation
                };

                var response = new PatientBL().RegisterPatient(newPatient);
                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }
        }
        public ActionResult GetPatient(int id)
        {
            var thisPatient = new PatientBL().GetRegisteredPatient().ToList().Find(x => x.PatientAutoID.Equals(id));
            return Json(thisPatient, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [SessionAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult ModifyPatient(ViewPatient obj)
        {
            try
            {

                var thisPatient = new PatientBL.PatientMetaData
                {

                    PatientAutoID = obj.PatientAutoID,
                    FirstName = Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.FirstName)),
                    LastName = Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.LastName)),
                    PatientIDNumber = obj.PatientIDNumber,
                    PatientIDType = obj.PatientIDType,
                    Gender = obj.Gender,
                    BirthDate = obj.BirthDate,
                    CurrentAddress = Common.textInfo.ToTitleCase(Helpers.SanitiseInput(obj.CurrentAddress)),
                    Occupation = obj.Occupation

                };


                var response = new PatientBL().ModifyPatient(thisPatient);
                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }
        }

        [SessionAuthorize]
        public ActionResult DeactivatePatient(int id)
        {
            try
            {

                var response = new PatientBL().DeactivatePatient(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }

        }

        [SessionAuthorize]
        public ActionResult ActivatePatient(int id)
        {
            try
            {

                var response = new PatientBL().ActivatePatient(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }

        }

        [SessionAuthorize]
        public ActionResult DeletePatient(int id)
        {

            try
            {

                var response = new PatientBL().DeletePatient(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }

        }

      

        [SessionAuthorize]
        public ActionResult CreateMedicalRecord (string id)
        {
            //Get diagnosis codes

            var diagnosisCodes = new PatientBL().GetDiagnosisCode().ToList();
            ViewBag.DiagnosisCodes = new SelectList(diagnosisCodes, "DiagnosisCodeID", "DiagnosisCodeDescription");



            try
            {
                int patientAutoID = Convert.ToInt32(Helpers.Decrypt(id));
                Session["PatientAutoID"] = patientAutoID;

                var patientDetail = new PatientBL().GetRegisteredPatient().ToList().Find(x => x.PatientAutoID == patientAutoID);
                Session["Patient"] = patientDetail.FirstName + " " + patientDetail.LastName + " with ID No. " + patientDetail.PatientIDNumber;
                
            }
            catch (Exception)
            {

            }

            
            return View();
        }

        [HttpPost]
        [SessionAuthorize]
        [ValidateAntiForgeryToken]

        public ActionResult CreateMedicalRecord(ViewMedicalVisit obj)
        {
            try
            {

                var newPatientVisit = new PatientBL.MedicalVisitMetaData
                {

                    PatientAutoID = Convert.ToInt32(Session["PatientAutoID"]),
                    VisitDate = obj.VisitDate,
                    Weight = obj.Weight,
                    Height = obj.Height,
                    Temperature = obj.Temperature,
                    DiagnosisCodeID = obj.DiagnosisCodeID,
                };

                var response = new PatientBL().NewMedicalRecord(newPatientVisit);
                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManagePatient");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManagePatient");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManagePatient");
            }
        }

        [SessionAuthorize]
        public ActionResult ViewMedicalRecord(string id)
        {
            try
            {
                int patientAutoID = Convert.ToInt32(Helpers.Decrypt(id));

                IEnumerable<ViewMedicalVisit> patientVisits = new PatientBL().GetPatientRecord().ToList().FindAll(x => x.PatientAutoID == patientAutoID);

                var patientDetail = new PatientBL().GetRegisteredPatient().ToList().Find(x => x.PatientAutoID == patientAutoID);
                Session["Patient"] = patientDetail.FirstName + " " + patientDetail.LastName + " with ID No. " + patientDetail.PatientIDNumber;


                if (patientVisits.Count() > 0)
                {
                    ViewBag.PatientMedicalRecords = patientVisits.ToList().OrderByDescending(x=>x.VisitDate);
                }

            }

            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
            }
            return View();
        }
    }
}