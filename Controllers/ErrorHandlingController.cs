﻿using System;
using System.Web.Mvc;
using Patient_Management_Project;

namespace Patient_Management_Project.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Fail()
        {
            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;
            Common.LogSystemError(Response.ToString() + "|" + DateTime.Now + "|" + Session["Username"]);

            return View();
        }
       
        public ActionResult Index()
        {
            Response.TrySkipIisCustomErrors = true;
            Common.LogSystemError(Response.ToString() + "|" + DateTime.Now + "|" + Session["Username"]);

            return View();
        }

    }
}