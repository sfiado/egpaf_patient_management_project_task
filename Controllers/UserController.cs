﻿using System;
using System.Linq;
using System.Web.Mvc;
using Patient_Management_Project.Functions;
using Patient_Management_Project.Models;
using static Patient_Management_Project.Helpers.SessionManager;
using Patient_Management_Project.Models.DAL;

namespace Patient_Management_Project.Controllers
{
    public class UserController : Controller
    {
        
       
        [SessionAuthorize]
        public ActionResult ManageUser()
        {

            try
            {
                var userID = LoginCache[(UserKey)Session["UserKey"]].UserID;
                var users = new Authentication().GetUsers(userID);

                ViewBag.Users = users;

                return View();
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult UserRegister(ViewUser obj)
        {

            var profileRequest = new Authentication.UserMetaData
            {

                FirstName = Common.textInfo.ToTitleCase(obj.FirstName),
                LastName = Common.textInfo.ToTitleCase(obj.LastName),
                EmailAddress = obj.EmailAddress.ToLower(),
                Username = $"{obj.FirstName.Substring(0, 1).Trim().ToLower()}{Helpers.SanitiseInput(obj.LastName.Trim()).ToLower()}", // generate a username
                UserPassword = obj.InitialPassword,
                UserRole = obj.UserRole,
                CreatedOn = DateTime.Now,
                CreatedBy = "Self Register",
                Active = true


            };


            try
            {
                var response = new Authentication().CreateUserProfile(profileRequest);
                if (!string.IsNullOrEmpty(response.Username))
                {
                    ModelState.Clear();
                    Session["ErrorMessage"] = string.Empty;
                    Session["SuccessMessage"]= string.Format(Messages.USER_REGISTERED_SUCCESSFULLY, response.Username);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    Session["SuccessMessage"] = string.Empty;
                    Session["ErrorMessage"] = response.PossibleErrorMessage;
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
               
                return RedirectToAction("Index", "Home");
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionAuthorize]
        public ActionResult NewUser(ViewUser obj)
        {

            if (ModelState.IsValid)
            {
                var profileRequest = new Authentication.UserMetaData
                {

                    FirstName = Common.textInfo.ToTitleCase(obj.FirstName),
                    LastName = Common.textInfo.ToTitleCase(obj.LastName),
                    EmailAddress = obj.EmailAddress.ToLower(),
                    Username = $"{obj.FirstName.Substring(0, 1).Trim().ToLower()}{Helpers.SanitiseInput(obj.LastName.Trim()).ToLower()}", // generate a username
                    UserPassword = obj.InitialPassword,
                    UserRole= obj.UserRole,
                    CreatedOn = DateTime.Now,
                    CreatedBy= LoginCache[(UserKey)Session["UserKey"]].Username,
                    Active = true


                };


                try
                {
                    var response = new Authentication().CreateUserProfile(profileRequest);
                    if (!string.IsNullOrEmpty(response.Username))
                    {
                        ModelState.Clear();
                        ViewBag.SuccessMessage = string.Format(Messages.USER_CREATED, response.Username);
                        return RedirectToAction("ManageUser");
                    }
                    else
                    {
                        ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    }
                }
                catch (Exception)
                {
                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                }
            }
            return RedirectToAction("ManageUser");

        }

        public ActionResult GetUser(int id)
        {
            try
            {
                var thisUser = new Authentication().GetUsers(LoginCache[(UserKey)Session["UserKey"]].UserID).ToList().Find(x => x.UserID == id);
                return Json(thisUser, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

            }

            return View();
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionAuthorize]
        public ActionResult ModifyUser(ViewUser obj)
        {
            try
            {
                var userID = LoginCache[(UserKey)Session["UserKey"]].UserID;
                
                var thisUser = new Authentication.ModifyUserMetaData
                {
                    UserID = obj.UserID,
                    Username = obj.Username,
                    EmailAddress = obj.EmailAddress,
                    FirstName = Common.textInfo.ToTitleCase(obj.FirstName),
                    LastName = Common.textInfo.ToTitleCase(obj.LastName),
                    UserRole = obj.UserRole
                };
                var response = new Authentication().ModifyUserProfile(thisUser);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManageUser");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManageUser");
                }


            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManageUser");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionAuthorize]
        public ActionResult ResetPassword(ViewUser obj)
        {
            try
            {

                var resetPassword = new Authentication.ResetUserPasswordMetaData
                {
                    NewPassword = obj.InitialPassword,
                    ConfirmNewPassword = obj.ConfirmNewPassword,
                    UserID = Convert.ToInt32(obj.UserID),
                    Username = obj.Username

                };

                var response = new Authentication().ResetPassword(resetPassword);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManageUser");
                }
                else
                {
                    ViewBag.ErrorMessage = response.PossibleErrorMessage;
                    return RedirectToAction("ManageUser");
                }

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                return RedirectToAction("ManageUser");
            }

        }

        [SessionAuthorize]
        public ActionResult DeactivateUser(int id)
        {
            try
            {
                
                var response = new Authentication().DeactivateUser(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManageUser");
                }
                else
                {
                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                    return RedirectToAction("ManageUser");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("ManageUser");
            }

        }

        [SessionAuthorize]
        public ActionResult ActivateUser(int id)
        {
            try
            {
                var response = new Authentication().ActivateUser(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManageUser");
                }
                else
                {
                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                    return RedirectToAction("ManageUser");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("ManageUser");
            }

        }

        [SessionAuthorize]
        public ActionResult DeleteUser(int id)
        {
            try
            {
                var response =new Authentication().DeleteUser(id);

                if (response.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = string.Format(Messages.OPERATION_COMPLETED_SUCCESSFULLY);
                    return RedirectToAction("ManageUser");
                }
                else
                {
                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                    return RedirectToAction("ManageUser");
                }

            }
            catch (Exception ex)
            {
                Common.LogEvent(ex.Message + "-Delete User|" + DateTime.Now);
                return RedirectToAction("ManageUser");
            }

        }


    }
}