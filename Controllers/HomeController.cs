﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using Patient_Management_Project.Models;
using Patient_Management_Project.Functions;
using static Patient_Management_Project.Helpers.SessionManager;
using static Patient_Management_Project.Models.DAL.Authentication;
using System.Linq;


namespace Patient_Management_Project.Controllers
{
    public class HomeController : Controller
    {
        private Thread loginThread;

        [AllowAnonymous]
        public ActionResult Index()
        {

            loginThread = new Thread(new ThreadStart(AutoKillExpiredSessions));
            try
            {
                
                loginThread.Start();
            }
            catch (Exception) { }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(Login obj)
        {

            if (ModelState.IsValid)
            {
               
                    var loginRequest = new AuthenticationMetaData
                    {
                        Username = Helpers.SanitiseInput(obj.Username),
                        Password = obj.Password,
                    };
                    try
                    {
                        var lockResponse = new Models.DAL.Authentication().IsProfileLocked(obj.Username);
                        if (lockResponse.IsLocked && lockResponse.UnlockTime > DateTime.Now)
                        {
                            // valid username but the profile is locked, stop here
                            TimeSpan span = lockResponse.UnlockTime.Value.Subtract(DateTime.Now);
                            if (span.Minutes > 1)
                            {
                                ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Minutes + 1, "minutes");
                            }
                            else if (span.Seconds > 0 && span.Minutes == 0)
                            {
                                ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Seconds, "seconds");
                            }
                            else if (span.Minutes == 1 && span.Seconds > 0)
                            {
                                ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Minutes, "minute");
                            }
                            else
                            {
                                ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Seconds, "seconds");
                            }
                            return View();
                        }

                        var authResponse = new Models.DAL.Authentication().AuthenticateUserLogin(loginRequest);
                        string ipAddress = Helpers.GetIPAddress();

                        // log this attempt, whether successful or not
                        new Models.DAL.Authentication().LogActivity(obj.Username, ipAddress, authResponse.Value.IsValidLogin);

                        if (authResponse.HasValue)
                        {
                            if (authResponse.Value.IsValidLogin)
                            {
                                if (authResponse.Value.Active <= 0)
                                {
                                    ViewBag.ErrorMessage = Messages.PROFILE_INACTIVE;
                                    return View();
                                }

                                // check whether the profile is locked or not
                                if (authResponse.Value.UnlockTime > DateTime.Now)
                                {
                                    // calculate the time the profile will be locked out
                                    TimeSpan span = authResponse.Value.UnlockTime.Subtract(DateTime.Now);
                                    if (span.Minutes > 1)
                                    {
                                        ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Minutes + 1, "minutes");
                                    }
                                    else if (span.Seconds > 0 && span.Minutes == 0)
                                    {
                                        ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Seconds, "seconds");
                                    }
                                    else if (span.Minutes == 1 && span.Seconds > 0)
                                    {
                                        ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Minutes, "minute");
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = string.Format(Messages.LOCKOUT, span.Seconds, "seconds");
                                    }
                                }
                                else
                                {
                                    // unlock the profile, lock time threshold exceeded
                                    new Models.DAL.Authentication().UnlockProfile(obj.Username);
                                }

                                int temp = 0;

                                // remove from the cache the wrong login attempts
                                LoginAttemptsCache.TryRemove(obj.Username, out temp);

                                var userKey = new UserKey
                                {
                                    IPAddress = ipAddress,
                                    Username = authResponse.Value.Username,
                                    Token = Guid.NewGuid(),
                                    UserAgent = HttpContext.Request.UserAgent
                                };

                                var userValue = new UserInfo
                                {
                                    Username = authResponse.Value.Username,
                                    UserID = authResponse.Value.UserID.GetValueOrDefault(),
                                    FirstName = authResponse.Value.FirstName,
                                    LastName = authResponse.Value.LastName,
                                    ConnString = DBConnect.MainConnectionString,
                                    UniqueToken = userKey.Token,
                                    UserRole = Helpers.Decrypt(authResponse.Value.UserRole),
                                    TokenExpiryTime = DateTime.Now.Add(new TimeSpan(0, Properties.Settings.Default.AutoKillTime, 0)),
                                    HasPasswordExpired = authResponse.Value.HasPasswordExpired,
                                    FirstLogin = authResponse.Value.FirstLogin.GetValueOrDefault(),
                                    EmailAddress = authResponse.Value.EmailAddress
                                };


                                Session.Add("UserKey", userKey);
                                Session.Add("Username", userValue.Username);
                                Session.Add("FirstName", userValue.FirstName);
                                Session.Add("LastName", userValue.LastName);
                                Session.Add("UserRole", userValue.UserRole);
                                Session.Add("PasswordExpiryDate", Helpers.ConvertToFriendlyDate(authResponse.Value.PasswordExpiryDate.GetValueOrDefault()));
                                

                                // check the last login date
                                if (authResponse.Value.LastLogin == new DateTime(1900, 1, 1) || authResponse.Value.LastLogin == new DateTime(1990, 1, 1))
                                {
                                    Session.Add("LastLogin", "Never");
                                }
                                else
                                {
                                    Session.Add("LastLogin", Helpers.ConvertToFriendlyDate(authResponse.Value.LastLogin));
                                }

                            // check the user role

                            var userRole = Helpers.Decrypt(authResponse.Value.UserRole);
                                switch (userRole.ToLower())
                                {
                                    case "administrator":
                                        Session["IsAdmin"] = true;
                                        break;
                                    case "clerk":
                                        Session["IsClerk"] = true;
                                        break;
                                    // Log them out
                                    default:
                                        Session["IsNoUser"] = true;
                                        break;
                                }

                                try
                                {
                                    // register this session in our dictionary of "sessions"
                                    RegisterNewSession(userKey, userValue);
                                    new Models.DAL.Authentication().UpdateLoginTime(userValue.Username, DateTime.Now);
                                }
                                catch (Exception)
                                {
                                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                                    return View("Index");
                                }

                                ViewBag.FirstName = userValue.FirstName;
                                ViewBag.LastName = userValue.LastName;

                                try
                                {
                                    // check if the password has expired or it's a first login
                                    // redirect to the change password view if need be
                                    if (authResponse.Value.HasPasswordExpired)
                                    {
                                        ViewBag.WarningMessage = Messages.PASSWORD_EXPIRED;
                                        return View("ChangePassword");
                                    }
                                    else if (authResponse.Value.FirstLogin > 0)
                                    {
                                        ViewBag.WarningMessage = Messages.FIRST_LOGIN;
                                        return View("ChangePassword");
                                    }
                                    else
                                    {
                                        return RedirectToAction("Dashboard");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ViewBag.ErrorMessage = ex.Message;
                                }
                                return RedirectToAction("Dashboard");
                            }
                            else
                            {
                                // keep track of the wrong login attempts...
                                if (LoginAttemptsCache.ContainsKey(obj.Username))
                                {
                                    int attempts = 0;
                                    LoginAttemptsCache.TryRemove(obj.Username, out attempts);
                                    attempts += 1;
                                    LoginAttemptsCache.TryAdd(obj.Username, attempts);

                                    // threshold has been reached...
                                    if (attempts >= Properties.Settings.Default.MaxLoginAttempts)
                                    {
                                        DateTime UnlockTime = DateTime.Now.Add(new TimeSpan(0, Properties.Settings.Default.LockDurationMinutes, 0));
                                        new Models.DAL.Authentication().LockProfile(obj.Username,UnlockTime);
                                    }
                                }
                                else
                                {
                                    LoginAttemptsCache.TryAdd(obj.Username, 1);
                                }
                                ViewBag.ErrorMessage = authResponse.Value.PossibleErrorMessage;
                            }
                        }

                    }
                    catch (Exception)
                    {
                        ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                        return View("Index");
                    }
               
                return View();
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            if (Session["UserKey"] != null)
            {
                // this is an explicit session end...
                KillSession((UserKey)Session["UserKey"]);
            }
            Session.Clear();
            Session.Abandon();
           
            return RedirectToAction("Index");
        }

        public ActionResult ChangePassword()
        {
            
            if (Session["UserKey"] == null)
            {
                return RedirectToAction("Index");
            }

            var userDetails = LoginCache[(UserKey)Session["UserKey"]];
            if (userDetails.HasPasswordExpired)
            {
                ViewBag.WarningMessage = Messages.PASSWORD_EXPIRED;
            }
            else if (userDetails.FirstLogin > 0)
            {
                ViewBag.WarningMessage = Messages.FIRST_LOGIN;
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult ChangePassword(ViewChangePassword obj)
        {

            if (Session["UserKey"] == null)
            {
                return RedirectToAction("Index");
            }

            bool checkPasswordContainsSymbol = Properties.Settings.Default.PasswordShouldContainSymbol;
            bool checkPasswordContainsNumber = Properties.Settings.Default.PasswordShouldContainNumber;
            bool checkPasswordContainsUpperCase = Properties.Settings.Default.PasswordShouldContainUpperCase;
            int minPasswordLength = Properties.Settings.Default.MinPasswordLength;

            var userDetails = LoginCache[(UserKey)Session["UserKey"]];

            if (ModelState.IsValid)
            {
                ViewBag.WarningMessage = string.Empty;

                if (obj.NewPassword.Length < minPasswordLength)
                {

                    ViewBag.ErrorMessage = string.Format(Messages.POLICY_NOT_MET, "Minimum Password Length Not Met");
                    return View();

                }


                if (checkPasswordContainsNumber)
                {
                    if (!Helpers.PasswordContainsNumber(obj.NewPassword))
                    {
                        ViewBag.ErrorMessage = string.Format(Messages.POLICY_NOT_MET, "Number Not Present");
                        return View();
                    }

                }
                if (checkPasswordContainsSymbol)
                {
                    if (!Helpers.PasswordContainsSymbol(obj.NewPassword))
                    {
                        ViewBag.ErrorMessage = string.Format(Messages.POLICY_NOT_MET, "Symbol Not Present");
                        return View();
                    }
                }
                if (checkPasswordContainsUpperCase)
                {
                    if (!Helpers.PasswordContainsUppercase(obj.NewPassword))
                    {
                        ViewBag.ErrorMessage = string.Format(Messages.POLICY_NOT_MET, "Uppercase Character Not Present");
                        return View();
                    }
                }
                try
                {
                    var authentication = new Models.DAL.Authentication();
                    if (authentication.IsCurrentPasswordValid(userDetails.Username, obj.CurrentPassword))
                    {
                        // proceed to change the password
                        if (authentication.ChangeUserPassword(userDetails.Username,obj.CurrentPassword, obj.NewPassword))
                        {
                            ViewBag.SuccessMessage = Messages.PASSWORD_CHANGED;
                            ViewBag.WarningMessage = string.Empty;
                            ModelState.Clear();
                        }
                        else
                        {
                            ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = Messages.CURRENT_PASSWORD_INCORRECT;
                    }
                }
                catch (Exception)
                {
                    ViewBag.ErrorMessage = Messages.GENERAL_ERROR;
                }
            }
            if (userDetails.HasPasswordExpired)
            {
                ViewBag.WarningMessage = Messages.PASSWORD_EXPIRED;
            }
            return View();
        }

        [SessionAuthorize]
        public ActionResult Dashboard()
        {
            return View();
        }

    }
}